#' @title comp_genescore
#'
#' @description
#' Compliment to genescore(), calculates subset of brainspan containing refid,
#' and genes found in both observed and brainspan datasets, and applies
#' genescore function row-wise across brainspan subset.
#'
#' @param obs_dt data.table, col1 = geneid, col2 = gene p-value
#' @param brainspan Entire brainspan correlation file for refid gene
#' @param spec_clusters Vector of specific cluster gene list char. vectors
#' @param refid Reference gene ID
#' @param adjustment Defaults to "gs", must be either "p" or "gs", determines
#' inflation correction method used in Genescore calculation
#'
#' @examples
#' refid = 83719
#' obs_dt <- res_pvals
#' spec_clusters <- make_specific_clusters()
#' brainspan <- read_brainspan()
#' adjustment <- "gs"
#' obs_gs <- comp_genescore(obs_dt, brainspan, spec_clusters, refid, adjustment)
#' @export
comp_genescore <- function(obs_dt, brainspan, spec_clusters, refid){
  #Pull p value for reference gene in passed permutation/observed result
  genep <- obs_dt[obs_dt[[1]] == refid, 2][[1]]

  #Set gs as empty vector to begin
  gs <- rep(NA, length(spec_clusters))

  #Score gene for each cluster, apply across entire brainspan correlation file
  for (i in 1:length(spec_clusters)){
    cluster = spec_clusters[[i]]
    brainspan_subset <- brainspan[[i]]

    #Calculate for every row in brainspan data
    gs_sum_vec <- c(0,0)
    for (j in 1:nrow(brainspan_subset)){
      single_score <- genescore(brainspan_subset[j,],
                                obs_dt = obs_dt,
                                refid = refid)
      gs_sum_vec[1] <- gs_sum_vec[1] + single_score[1]
      gs_sum_vec[2] <- gs_sum_vec[2] + single_score[2]
    }

    #Calculate score from GS sum and number of genes
    score <- ((-log10(genep))+gs_sum_vec[1])/(gs_sum_vec[2]+1)

    #Concatenate single cluster GS to vector
    gs[i] <- score
  }
  #Return sum vector
  return(gs)
}
