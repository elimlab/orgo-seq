#' @title gene_match_pval
#'
#' @description
#' Pulls p-values from observed data for genes in a cluster
#' Returns a numeric vector of pvalues of matching genes in cluster
#' Used in tandem with comp_cellscore()
#'
#' @param obs_p 2 column data table, col1 = gene name, col2 = p-value.
#' @param cluster Vector of gene names from scRNA-Seq cluster.
#'
#' @examples
#' obs_p <- data.table::fread("observed_data.csv")
#' cluster <- data.table::fread("cluster1.txt")
#' p_val_vec <- gene_match_pval(obs_p, cluster)
gene_match_pval <- function(obs_p, cluster){
  pval_match <- obs_p[obs_p[[1]] %in% cluster,2]
  pval_match <- pval_match[[1]]
  return(pval_match)
}
